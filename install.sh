sudo apt update
sudo apt install git -y
sudo cd /var/www/html
sudo rm -rf probni-projekat
sudo git clone https://uarnautsingidunum@bitbucket.org/uarnautsingidunum/probni-projekat.git
sudo chown www-data:singidunum probni-projekat -R
sudo cat /etc/apache2/sites-available/000-default.conf | sed 's/#ServerName www.example.com/ServerName probni.projekat/' | sed 's@/var/www/html@/var/www/html/probni-projekat@' > ~/probni-projekat.conf
sudo mv ~/probni-projekat.conf /etc/apache2/sites-available/probni-projekat.conf
sudo a2ensite probni-projekat
sudo systemctl reload apache2
sudo mysql -e "DROP DATABASE IF EXISTS probni_projekat;";
sudo mysql -e "CREATE DATABASE probni_projekat;";
sudo mysql -e "GRANT ALL ON probni_projekat.* TO 'probni'@'localhost' IDENTIFIED BY 'ProbniProjekat!1248';";
sudo mysql -e "FLUSH PRIVILEGES;";
sudo mysql < /var/www/html/probni-projekat/prepare.sql
